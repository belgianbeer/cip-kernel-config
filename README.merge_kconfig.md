# Merge cip kernel configs

The script (kconfig.py)[./scripts/kconfig.py] is can be used to merge multiple kernel
configuration files together. It is based on
https://github.com/zephyrproject-rtos/zephyr/blob/main/scripts/kconfig/kconfig.py.

This kconfig.py use [kconfiglib](https://github.com/ulfalizer/Kconfiglib/blob/master/kconfiglib.py) to for parsing and merging.

To merge the kernel configuration of one architecture and kernel version execute the
helper script kconfig_cip_merge.sh:

```bash
./scripts/kconfig_cip_merge.sh <path to Linux kernel> <architecture> <kernel version> [additional configurations]
```
e.g. for the x86 kernel version 6.1-cip with the kernel repo at ../linux:
```bash
./scripts/kconfig_cip_merge.sh ../linux/ x86 6.1-cip merge_fixups/x86_fixes.cfg
```

## Kconfiglib

To work with kernels > 5.13 the Kconfiglib needs to from Debian testing
or contain the following patch https://github.com/ulfalizer/Kconfiglib/pull/119.
