#!/usr/bin/env bash
set -e
usage() {
	echo "This script merges the cip kernel configurations into one."
	echo "Usage: $0 <kernel_dir> <arch> <kernel_version> [kconfig_files]"
	echo "  kernel_dir: path to the kernel source directory"
	echo "  arch: architecture of the kernel"
	echo "  kernel_version: version of the kernel"
	echo "  kconfig_files: list of kconfig files to merge"
	exit 1
}

check_config_repo() {
	if [ ! -d "$KERNEL_CONFIG_REPO" ]; then
		git clone "$KERNEL_CONFIG_REPO" "$current_dir"/cip-kernel-config
		KERNEL_CONFIG_REPO="$current_dir"/cip-kernel-config
	fi
	configs=$(find "$KERNEL_CONFIG_REPO"/"$KERNEL_VERSION".y*"$KERNEL_VERSION_POSTFIX"/"$ARCH" -name "*_defconfig")
}

script_dir=$(dirname "$(readlink -f "$0")")
current_dir=$(pwd)
KERNEL_DIR="$1"
ARCH="$2"
KERNEL_VERSION="$3"

case $KERNEL_VERSION in
	*cip|*rt)
		KERNEL_VERSION_POSTFIX=${KERNEL_VERSION#*-}
		KERNEL_VERSION=${KERNEL_VERSION%%-*}
		;;
	*)
		true
		;;
esac

KERNEL_CONFIG_REPO="${KERNEL_CONFIG_REPO:-.}"
if [ -z "$KERNEL_DIR" ] || [ -z "$ARCH" ] || [ -z "$KERNEL_VERSION" ]; then
	usage
fi
DISABLE_DIFFS="false"
case  "$#" in
	1|2)
		usage
		;;
	3)
		check_config_repo
		;;
	*)
		shift 3
		if [ -n "$KERNEL_CONFIG_REPO" ]; then
			check_config_repo
		fi
		configs="$configs $*"
		;;
esac
export 	ARCH
export 	SRCARCH="$ARCH"

# we nee to export these variables to make kconfig.py work
# as the variables are used by the  kernel Kconfig files
# as the content is not important set them to some sane dummy values
export  CC=gcc
export  LD=ld
export  OBjCOPY=objcopy
export  RUSTC=rustc
export  BINDGEN=bindgen
export  KSRC=$KERNEL_DIR
export  srctree=$KERNEL_DIR

if [ ! -d "$KERNEL_DIR" ]; then
	echo "Kernel directory $KERNEL_DIR does not exist"
	exit 1
fi
kernel_tags=$(git  -C "$KERNEL_DIR" tag -l "v$KERNEL_VERSION*")
case  $KERNEL_VERSION_POSTFIX  in
	*cip)
		filtered_tags=$(echo "$kernel_tags" | grep -E '^.*-cip[0-9]+$')
		;;
	*cip-rt)
		filtered_tags=$(echo "$kernel_tags" | grep -E '^.*-cip[0-9]+-rt[0-9]+$')
		;;
	*rt)
		filtered_tags=$(echo "$kernel_tags" | grep -E '^.*-rt[0-9]+$')
		;;
	"")
		filtered_tags=$(echo "$kernel_tags" | grep -E '^v[0-9]+.[0-9]+.?[0-9]*$')
		;;
	*)
		echo "unkown $KERNEL_VERSION_POSTFIX"
		;;
esac
merge_identifer="${KERNEL_VERSION}_${KERNEL_VERSION_POSTFIX}_${ARCH}"
latest_kernel_tag=$( echo "$filtered_tags" | sort -V | tail -n 1)
git  -C "$KERNEL_DIR" checkout "$latest_kernel_tag"
"$script_dir"/kconfig.py "$KERNEL_DIR"/Kconfig --handwritten-input-configs \
	"$current_dir/merged_config_$merge_identifer" \
	"$current_dir/parsed_kconfig_files_$merge_identifer" \
	$configs 2>&1 | tee -p "$current_dir"/merged_config_"$merge_identifer".log

cp  "$current_dir/merged_config_$merge_identifer" "$KERNEL_DIR"/.config
pushd "$KERNEL_DIR" || exit 1
make  savedefconfig
cp  defconfig "$current_dir/merged_defconfig_$merge_identifer"
popd || exit 1

if [ ${DISABLE_DIFFS} = "true" ]; then
	exit 0
fi
diff_location="$current_dir"/"$KERNEL_VERSION${KERNEL_VERSION_POSTFIX}"

mkdir -p "${diff_location}"
for  config in $configs; do
	config_name="$(basename "$config")"

	case $config_name in
		*.cfg)
			continue
			;;
		*)
			true
			;;
	esac
	cp "$config" "$KERNEL_DIR"/arch/"$ARCH"/configs/"$config_name"
	pushd "$KERNEL_DIR" || exit 1
	make "$config_name"
	"$KERNEL_DIR"/scripts/diffconfig "$KERNEL_DIR"/.config "$current_dir/merged_config_${merge_identifer}"  > \
		"$diff_location/diff_config_${merge_identifer}_$(basename "$config")"
	popd || exit 1
	"$KERNEL_DIR"/scripts/diffconfig "$config" "$current_dir/merged_defconfig_${merge_identifer}"  > \
		"$diff_location/diff_defconfig_${merge_identifer}_$(basename "$config")"
done
